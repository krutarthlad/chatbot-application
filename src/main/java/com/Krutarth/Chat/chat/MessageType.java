package com.Krutarth.Chat.chat;

public enum MessageType {

    CHAT,
    JOIN,
    LEAVE
}
